// JavaScript Document
(function($) {

	$(document).foundation();

    $(document).ready(function() {
 
    	// Modernizr tests (Because since FF26 .touch is no longer a reliable way of distinguishing between mobile and desktop)
				Modernizr.addTest('firefox', function () {
					 return !!navigator.userAgent.match(/firefox/i);
					});
				Modernizr.addTest('chrome', function () {
					 return !!navigator.userAgent.match(/chrome/i);
					});
				Modernizr.addTest('safari', function () {
					 return !!navigator.userAgent.match(/safari/i);
					});
				Modernizr.addTest('mobile-agent', function () {
					 return !!navigator.userAgent.match(/android|webos|iphone|ipad|ipod|iemobile|opera mini|IEMobile|BlackBerry/i);
					});
				Modernizr.addTest('ios', function () {
					 return !!navigator.userAgent.match(/iphone|ipad|ipod/i);
					});
				Modernizr.addTest('android', function () {
					 return !!navigator.userAgent.match(/android/i);
					});

	$.ajaxSetup({
		cache: true
	});

	$(document).foundation({
	  offcanvas : {
	    open_method: 'overlap_single', // Sets method in which offcanvas opens, can also be 'overlap'
	    close_on_click : false
	  }
	});

	$(window).scroll(function() {
		$(".no-ios .off-canvas-list").css("margin-top", $(window).scrollTop());
		if ($(".off-canvas-wrap.move-right").length > 0 ) {
			$(".tab-bar").css("top", $(window).scrollTop());
			//$(".left-off-canvas-menu").css("top", $(window).scrollTop());
		} else {
			$(".tab-bar").css("top", 0);
			//$(".left-off-canvas-menu").css("top", 0);
		}
	});

	$(document).on('open.fndtn.offcanvas', '[data-offcanvas]', function() {
	  $(".tab-bar").css("top", $(window).scrollTop());
	  //$(".left-off-canvas-menu").css("top", $(window).scrollTop());
	});
	$(document).on('close.fndtn.offcanvas', '[data-offcanvas]', function() {
	  $(".tab-bar").css("top", 0);
	  //$(".left-off-canvas-menu").css("top", 0);
	});


	if ($("#instafeed").length > 0) {

		var feed = new Instafeed({
			get: 'user',
			userId: 1535434559,
		    accessToken: '1143307226.c8a6c22.34b25eda86804fc981f0927127d191ba',
			clientId: 'd85ef8079570426d890a50caf6cc2998',
			limit: '20',
			template: '<div><a href="{{link}}"><img src="{{image}}" /></a></div>',
			after: function() {
				instagramSlick();
			}
	    });
	    feed.run();

	    function instagramSlick(){
		    $('#instafeed').slick({
				speed: 900,
				autoplay: false,
			    dots: false,
			    arrows: true,
			    slidesToShow: 5,
  				slidesToScroll: 2,
  				infinite: true,
  				centerMode: true,
  				centerPadding: '60px',
				responsive: [
			    {
			      breakpoint: 960,
			      settings: {
			        speed: 900,
					autoplay: false,
					arrows: true,
					slidesToShow: 4,
	  				slidesToScroll: 2,
	  				infinite: true,
	  				centerMode: true,
  					centerPadding: '60px',
			      }
			    },
			    {
			      breakpoint: 769,
			      settings: {
			        speed: 600,
					autoplay: false,
					arrows: true,
					slidesToShow: 3,
	  				slidesToScroll: 1,
	  				infinite: true,
	  				centerMode: true,
  					centerPadding: '60px',
			      }
			    },
			    {
			      breakpoint: 567,
			      settings: {
			        speed: 600,
					autoplay: false,
					arrows: true,
					slidesToShow: 1,
	  				slidesToScroll: 1,
	  				infinite: true,
	  				centerMode: true,
  					centerPadding: '60px',
			      }
			    }
			  ]
			});
		} // end instagramSlick

	} // end if .instagram-feed

	// HOMEPAGE FUNCTIONS
	if ($(".homepage").length > 0) {
		
		if ($("#available-plots").length > 0) {
			$("#available-plots ul li a").hover(function() {
				var $plot_hover = $(this).find("i").text();
				$(this).closest(".plots").addClass( "hover-" + $plot_hover );
			}, function() {
				var $plot_hover = $(this).find("i").text();
				$(this).closest(".plots").removeClass( "hover-" + $plot_hover );
			}
			);
			$("#available-plots map area").hover(function() {
				var $plot_hover = $(this).attr("id").split("-").pop();
				$(this).closest(".plots").addClass( "hover-" + $plot_hover );
			}, function() {
				var $plot_hover = $(this).attr("id").split("-").pop();
				$(this).closest(".plots").removeClass( "hover-" + $plot_hover );
			}
			);
		}

		if(window.location.hash) {
			var scroll_to = $('html, body').scrollTop() - 30;
			if($(window).width() > 960) {
				$('html, body').scrollTop(scroll_to);
			} else {
				$('html, body').scrollTop(scroll_to);
			}
		}

		// navigation internal links
		$('#navglobal ul li a').on('click',function (e) {
			if (!$(this).parent().hasClass("about")) {
			    e.preventDefault();
			    var target = this.hash,
			    $target = $(target);	
			    if($(window).width() > 960) {
				    $('html, body').stop().animate({
				        'scrollTop': $target.offset().top - 34
				    }, 900, 'swing', function () {
				        window.location.hash = target;
				        $('html, body').scrollTop($target.offset().top - 34);
				    });
				} else {
					$('html, body').stop().animate({
				        'scrollTop': $target.offset().top - 24
				    }, 900, 'swing', function () {
				        window.location.hash = target;
				        $('html, body').scrollTop($target.offset().top - 24);
				    });
				}

			    $(".off-canvas-wrap").delay("900").removeClass("move-right");
			}
		});

	    //GOOGLE MAPS
	    function initialize() {
       
			 geocoder = new google.maps.Geocoder();
			 var latlng = new google.maps.LatLng(52.9519572,-0.8974491);
			 
			 var jpcStyle = [
			  {
			    "featureType": "landscape.natural",
			    "stylers": [
			      { "visibility": "on" },
			      { "color": "#f7f4ef" }
			    ]
			  },{
			    "featureType": "landscape.man_made",
			    "stylers": [
			      { "visibility": "on" },
			      { "color": "#f0ebe0" }
			    ]
			  },{
			    "featureType": "water",
			    "stylers": [
			      { "visibility": "on" },
			      { "color": "#acd9ea" }
			    ]
			  },{
			    "featureType": "transit.line",
			    "stylers": [
			      { "visibility": "on" },
			      { "color": "#bcbdbe" }
			    ]
			  },{
			  },{
			    "featureType": "poi",
			    "elementType": "geometry.fill",
			    "stylers": [
			      { "color": "#dceae1" }
			    ]
			  },{
			    "featureType": "road.highway",
			    "elementType": "geometry.fill",
			    "stylers": [
			      { "color": "#dededd" }
			    ]
			  },{
			    "featureType": "road.highway",
			    "elementType": "geometry.stroke",
			    "stylers": [
			      { "color": "#cdcccd" }
			    ]
			  },{
			    "featureType": "poi",
			    "elementType": "labels.text.fill",
			    "stylers": [
			      { "color": "#808080" }
			    ]
			  },{
			    "featureType": "poi",
			    "elementType": "labels.text.stroke",
			    "stylers": [
			      { "color": "#808080" },
			      { "weight": 0.1 }
			    ]
			  },{
			  },{
			    "featureType": "water",
			    "elementType": "labels.text.fill",
			    "stylers": [
			      { "color": "#808080" }
			    ]
			  },{
			    "featureType": "water",
			    "elementType": "labels.text.stroke",
			    "stylers": [
			      { "color": "#808080" },
			      { "weight": 0.1 }
			    ]
			  },{
			  },{
			  }
			]

			 
				var myOptions = {
          center: latlng,
          zoom: 10,
					panControl: false,
					mapTypeControl: false,
					streetViewControl: false,
					scrollwheel: false,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map_canvas"),
            myOptions);
				
				map.setOptions({styles: jpcStyle});
			
				var image = new google.maps.MarkerImage(
					'img/mapmarker.png',
					new google.maps.Size(27,37),
					new google.maps.Point(0,0),
					new google.maps.Point(16,37)
				);
			
				var shadow = new google.maps.MarkerImage(
					'img/shadow.png',
					new google.maps.Size(40,34),
					new google.maps.Point(0,0),
					new google.maps.Point(20,34)
				);
			
				var shape = {
					coord: [13,0,15,1,16,2,17,3,18,4,18,5,19,6,19,7,19,8,19,9,19,10,19,11,19,12,19,13,18,14,18,15,17,16,16,17,15,18,14,19,14,20,13,21,13,22,12,23,12,24,12,25,12,26,11,27,11,28,11,29,11,30,11,31,11,32,11,33,8,33,8,32,8,31,8,30,8,29,8,28,8,27,8,26,7,25,7,24,7,23,6,22,6,21,5,20,5,19,4,18,3,17,2,16,1,15,1,14,0,13,0,12,0,11,0,10,0,9,0,8,0,7,0,6,1,5,1,4,2,3,3,2,4,1,6,0,13,0],
					type: 'poly'
				};
				
				
				// Popup Balloon
			
				var contentString = '<div id="content">'+
						'<div id="siteNotice">'+
						'</div>'+
						'<div id="bodyContent">'+
						'<p>The Blomfield<br />'+
							'Main&nbsp;St,&nbsp;Aslockton<br />'+
							'Nottinghamshire<br />'+
							'NG13</p>'+
						'</div>'+
						'</div>';
				
				var infowindow = new google.maps.InfoWindow({
						content: contentString,
						maxWidth: 450
				});
			
			
				function codeAddress() {

					geocoder.geocode( { location: latlng}, function(results, status) {
						if (status == google.maps.GeocoderStatus.OK) {
							map.setCenter(results[0].geometry.location);
							/*var marker = new google.maps.Marker({
									map: map,
									position: results[0].geometry.location
							});*/
							
							var marker = new google.maps.Marker({
								/*draggable: true,
								raiseOnDrag: false,*/
								icon: image,
								shadow: shadow,
								shape: shape,
								title:"The Blomfield",
								map: map,
								position: results[0].geometry.location
							});
							
							google.maps.event.addListener(marker, 'click', function() {
								infowindow.open(map,marker);
							});
							
						} else {
							alert("Geocode was not successful for the following reason: " + status);
						}
					});
				}

			codeAddress();

		}  // end initialize

		initialize();

	} // end if homepage

    }); /* end doc-ready */

})($);